#include "mem.h"
#include "mem_internals.h"
#include <assert.h>

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void alloc_test(void)
{
    void* heap = heap_init(0);
    assert(heap);
    void* block = _malloc(128);
    assert(block);
    heap_term();
}

void free_block_test(void)
{
    void* heap = heap_init(0);
    assert(heap);

    void* block1 = _malloc(128);
    assert(block1);

    void* block2 = _malloc(128);
    assert(block2);

    _free(block2);

    assert(!block_get_header(block1)->is_free);
    assert(block_get_header(block2)->is_free);

    heap_term();
}

void free_two_blocks_test(void)
{
    void* heap = heap_init(0);
    assert(heap);

    void* block1 = _malloc(128);
    assert(block1);

    void* block2 = _malloc(128);
    assert(block2);

    void* block3 = _malloc(128);
    assert(block3);

    _free(block2);

    assert(!block_get_header(block1)->is_free);
    assert(block_get_header(block2)->is_free);
    assert(!block_get_header(block3)->is_free);

    _free(block3);

    assert(!block_get_header(block1)->is_free);
    assert(block_get_header(block2)->is_free);
    assert(block_get_header(block3)->is_free);

    heap_term();
}

void extend_test(void)
{
    void* heap = heap_init(0);
    assert(heap);

    void* block1 = _malloc(128);
    assert(block1);

    void* block2 = _malloc(128);
    assert(block2);

    debug_heap(stdout, HEAP_START);

    void* block3 = _malloc(1024);
    assert(block3);

    debug_heap(stdout, HEAP_START);

    heap_term();
}

int main()
{
    alloc_test();
    printf("Videlenie - ok\n");
    free_block_test();
    printf("Free odin block - ok\n");
    free_two_blocks_test();
    printf("Free dwa blocka - ok\n");
    extend_test();
    printf("Rashirenie - ok\n");

    return 0;
}
